<?php

namespace Mgzaspuc\People\Seeder;

use Illuminate\Database\Seeder;
use DB;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            'name' => 'admin',
            'birthdate' => '1993-05-20',
            'phone' => '1132153215',
            'email' => 'admin@admin.com',
            'cpf' => '11111111111',
            'address' => 'Rua X',
            'address_number' => 1,
            'address_state' => 'PR',
            'address_city' => 'Maringá',
            'address_code' => '87083370',
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('people')->insert([
            'name' => 'Vendedor',
            'birthdate' => '1993-05-20',
            'phone' => '1132153215',
            'email' => 'seller@seller.com',
            'cpf' => '11111111112',
            'address' => 'Rua X',
            'address_number' => 1,
            'address_state' => 'PR',
            'address_city' => 'Maringá',
            'address_code' => '87083370',
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('people')->insert([
            'name' => 'Cliente',
            'birthdate' => '1993-05-20',
            'phone' => '1132153215',
            'email' => 'client@client.com',
            'cpf' => '11111111113',
            'address' => 'Rua X',
            'address_number' => 1,
            'address_state' => 'PR',
            'address_city' => 'Maringá',
            'address_code' => '87083370',
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
