<?php

namespace Mgzaspuc\People\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'birthdate' => 'required',
            'phone' => 'required|max:191',
            'email' => 'required|email|max:191',
            'cpf' => 'required|max:191',
            'address' => 'required|max:191',
            'address_number' => 'required|max:191',
            'address_complement' => 'max:191',
            'address_state' => 'required|max:191',
            'address_city' => 'required|max:191',
            'address_code' => 'required|max:191',
        ];
    }

    public function messages() {
        return [
            'name.required' => 'O campo Nome não pode ser vazio',
            'name.max' => 'A Nome não pode ter mais de 191 caracteres',
            'birthdate.required' => 'O campo Data de Aniversário não pode ser vazio',
            'phone.required' => 'O campo Telefone não pode ser vazio',
            'phone.max' => 'O Telefone não pode ter mais de 191 caracteres',                                         
            'email.required' => 'O campo Email não pode ser vazio',
            'email.email' => 'O Email informado é inválido',
            'email.max' => 'O Email não pode ter mais de 191 caracteres',                        
            'cpf.required' => 'O campo CPF não pode ser vazio',
            'cpf.max' => 'O CPF não pode ter mais de 191 caracteres',              
            'state_registration.required' => 'O campo Inscrição Estadual não pode ser vazio',
            'state_registration.max' => 'A Inscrição Estadual não pode ter mais de 191 caracteres',                 
            'address.required' => 'O campo Logradouro não pode ser vazio',            
            'address.max' => 'O Logradouro não pode ter mais de 191 caracteres',              
            'address_number.required' => 'O campo Número não pode ser vazio',            
            'address_number.max' => 'O Número não pode ter mais de 191 caracteres',               
            'address_complement.max' => 'O Complemento não pode ter mais de 191 caracteres',              
            'address_state.required' => 'O campo Estado não pode ser vazio',            
            'address_state.max' => 'O Estado não pode ter mais de 191 caracteres',                          
            'address_city.required' => 'O campo Cidade não pode ser vazio',            
            'address_city.max' => 'A Cidade não pode ter mais de 191 caracteres',                                      
            'address_code.required' => 'O campo CEP não pode ser vazio',            
            'address_code.max' => 'O CEP não pode ter mais de 191 caracteres',               
        ];   
    }
}
