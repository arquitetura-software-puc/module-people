<?php

namespace Mgzaspuc\People\Http\Controllers;

use App\Http\Controllers\Controller;
use Mgzaspuc\People\Http\Requests\UpdateRequest;
use Mgzaspuc\People\Http\Requests\StoreRequest;
use Illuminate\Http\Request;
use App\Http\Models\People;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $people = new People();
        $listPerson = $people->paginate(15);
        
        return view('people.index', compact('listPerson'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $people = new People();
        $listPerson = $people->all();
        return view('people.create', compact('listPerson'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try{        
            $person = new People();
            $person->name = $request->get('name');
            $person->birthdate = \DateTime::createFromFormat('d/m/Y', $request->get('birthdate'));
            $person->phone = $request->get('phone');            
            $person->email = $request->get('email');
            $person->cpf = $request->get('cpf');
            $person->address = $request->get('address');
            $person->address_number = $request->get('address_number');
            $person->address_complement = $request->get('address_complement');
            $person->address_state = $request->get('address_state');
            $person->address_city = $request->get('address_city');
            $person->address_code = $request->get('address_code');
            $person->created_at = new \DateTime();
            $person->updated_at = new \DateTime();
            $person->save();
        
            return redirect('/pessoas')
                ->with('success', 'Registro criado com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/pessoa/novo')
                ->with('error', 'Não foi possível criar o registro!' . $ex->getMessage())
                ->withInput($request->input());
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $people = new People();
        $person = $people->find($id);
        
        return view('people.edit', compact('person'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {        
        try{
            $people = new People();
            $person = $people->find($request->input('id'));     
            $person->name = $request->get('name');
            $person->birthdate = \DateTime::createFromFormat('d/m/Y', $request->get('birthdate'));
            $person->phone = $request->get('phone');            
            $person->email = $request->get('email');
            $person->cpf = $request->get('cpf');
            $person->address = $request->get('address');
            $person->address_number = $request->get('address_number');
            $person->address_complement = $request->get('address_complement');
            $person->address_state = $request->get('address_state');
            $person->address_city = $request->get('address_city');
            $person->address_code = $request->get('address_code');
            $person->updated_at = new \DateTime();
            $person->save();
        
            return redirect('/pessoas')
                ->with('success', 'Registro alterado com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/pessoa/editar/' . $request->input('id'))
                ->with('error', 'Não foi possível alterar o registro!');
        }
    }    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $people = new People();
            $person = $people->find($request->input('id'));        
            $person->delete();       
            
            return redirect('/pessoas')
                ->with('success', 'Registro excluído com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/pessoas')
                ->with('error', 'Não foi possível excluir o registro!');
        }
    }
}
