@extends('layouts.admin')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Pessoas</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Pessoas</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Menu</h3>
                    </div>
                    <div class="panel-body">
                        @include('elements.admin_menu')
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h2>Cadastrar Pessoa</h2>
                @include('elements.message_success_error')

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form method="post" action="{{ route('pessoa_salvar') }}">
                    @csrf
                    <div class="form-group">    
                        <label for="name">Nome:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="birthdate">Data de aniversário:</label>
                        <input type="text" class="form-control" id="birthdate" name="birthdate" value="{{old('birthdate')}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="phone">Telefone:</label>
                        <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone')}}"/>
                    </div>                         

                    <div class="form-group">    
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}"/>
                    </div>                    

                    <div class="form-group">    
                        <label for="cpf">CPF:</label>
                        <input type="text" class="form-control" id="cpf" name="cpf" value="{{old('cpf')}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="address">Logradouro:</label>
                        <input type="text" class="form-control" id="address" name="address" value="{{old('address')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="address_number">Número:</label>
                        <input type="text" class="form-control" id="address_number" name="address_number" value="{{old('address_number')}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="address_complement">Complemento:</label>
                        <input type="text" class="form-control" id="address_complement" name="address_complement" value="{{old('address_complement')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="address_city">Cidade:</label>
                        <input type="text" class="form-control" id="address_city" name="address_city" value="{{old('address_city')}}"/>
                    </div>                    

                    <div class="form-group">    
                        <label for="address_state">Estado:</label>
                        <input type="text" class="form-control" id="address_state" name="address_state" value="{{old('address_state')}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="address_code">CEP:</label>
                        <input type="text" class="form-control" id="address_code" name="address_code" value="{{old('address_code')}}"/>
                    </div>                    
                    
                    <button type="submit" class="btn btn-default">Cadastrar</button>
                </form>
            </div>                
        </div>
    </div>
</div>
</div>
<!-- GET IT-->
@endsection

@section('script')
<script type="text/javascript">
    $('[name="birthdate"]').mask('00/00/0000');
    $('[name="cpf"]').mask('000.000.000-00');
    $('[name="address_code"]').mask('00000-000');
    $('[name="phone"]').mask('(00) 0000-00009');
</script>
@endsection('script')