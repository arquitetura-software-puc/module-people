<?php

namespace Mgzaspuc\People;

use Illuminate\Support\ServiceProvider;

class ModulePeople extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/migrations' => base_path('database/migrations/')
        ]);

        $this->publishes([
            __DIR__.'/resources' => base_path('resources/views/')
        ]);

        $this->publishes([
            __DIR__.'/Seeder' => base_path('database/seeds/')
        ]);
    }
}