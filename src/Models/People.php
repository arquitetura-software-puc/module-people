<?php

namespace Mgzaspuc\People;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $table = 'people';

    public function user() {
        return $this->belongsTo('App\User','id_user','id');
    }
}
